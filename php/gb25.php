<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Access Restricted</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            background-color: #f8f9fa;
            font-family: Arial, sans-serif;
            text-align: center;
        }
        .container {
            max-width: 600px;
            padding: 20px;
            background: white;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
        }
        h1 {
            color: #d9534f;
        }
        p {
            color: #333;
        }
        ul {
            text-align: left;
            display: inline-block;
        }
        a {
            color: #007bff;
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Access Restricted</h1>
        <p>We regret to inform you that access to this website has been blocked from your current location. This restriction applies to users accessing from the <strong>Russian Federation</strong> or its allied nations.</p>
        <p>This decision has been made in <strong>solidarity with Ukraine</strong>, in response to the ongoing conflict and the actions taken by the Russian government and its supporters. We stand with those who support peace, sovereignty, and the international rule of law.</p>
        <p>For more information on how you can support Ukraine, consider visiting:</p>
        <ul>
            <li><a href="https://u24.gov.ua/" target="_blank">United24</a> – Ukraine’s official fundraising platform</li>
            <li><a href="https://savelife.in.ua/en/" target="_blank">Come Back Alive</a> – A foundation supporting Ukrainian defense efforts</li>
            <li><a href="https://www.icrc.org/en/where-we-work/europe-central-asia/ukraine" target="_blank">Red Cross Ukraine</a> – Providing humanitarian aid</li>
        </ul>
        <p>Thank you for your understanding.</p>
    </div>
</body>
</html>