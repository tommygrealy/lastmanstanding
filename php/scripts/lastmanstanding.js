/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var noFixToDisplayMsg = "There are currently no fixtures available for selection, \n\
    fixtures for the next round will be avilable for selection at the concusion of the current round of matches";

var userToView = "";
var currentUsername ="";
var user_dynamite_id = null
var dynamiteTargetUser = "";
var dynamiteTargetFullName = "";

$(document).on("pageinit", "#standings", function () {
    displayPlayerStandings();
});

$(document).on("pageshow", "#userHistory", function () {
    showPlayerHist(userToView);
});

$(document).on("pageinit", "#dynamite_page", function(){
    $.ajax({
        url: 'restServices/showUserDynamiteOptions.php',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if (response.length > 0 && response[0].status === 1) {
                localStorage.setItem('updatedAt', response[0].updated_at);
                user_dynamite_id = response[0].dynamite_id
                displayPlayersForDynamite()
                $('#no-dynamite-msg').hide()
                $('#no-dynamite-msg').show()
            }
        },
        error: function(xhr, status, error) {
            console.log('Error: unexpected response from ' + url + ', ' + error);
        }
    });
    showDynamiteHist();
})

var params = new window.URLSearchParams(window.location.search);

function formatDateTime(dateTimeString) {
    // Convert the string to a Date object
    const date = new Date(dateTimeString.replace(' ', 'T'));

    // Array to get the day name
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    // Extract the day
    const dayName = days[date.getDay()];

    // Extract the hours and format them for 12-hour clock
    let hours = date.getHours();
    const period = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12 || 12; // Convert 0 (midnight) to 12

    // Extract the minutes and add leading zero if needed
    const minutes = date.getMinutes().toString().padStart(2, '0');

    // Format minutes if they are non-zero
    const formattedTime = minutes === '00' ? `${hours}${period}` : `${hours}:${minutes}${period}`;

    // Combine the formatted date
    return `${dayName} at ${formattedTime}`;
  }

function loadUserOpts() {
    $.ajax({
        'url': 'restServices/showUserSelectionOptions.php',
        dataType: 'json',
        success: function (json) {
            if (json.userstatus) {
                currentUsername = json.userstatus.username
                if (json.userstatus.CompStatus == "Eliminated") {
                    console.log("Elim");
                    $("#elminiatedNotifyPopup").popup();
                    $("#elminiatedNotifyPopup").popup("open");
                    //return;
                }

                if (json.userstatus.PaymentStatus == "Pending") {
                    $("#paymentNotifyPopup").popup();
                    $("#paymentNotifyPopup").popup("open");
                    console.log("PayPend");
                    //return;
                }
            }

            if (json.fixtures) {
                $('#messageInformSelect').html("Please select one match winner from the list of fixtures below<br>")
                var AllowedTeams = json.availableTeams;
                console.log(JSON.stringify(AllowedTeams));
                $("#upComingFixtureList").empty();
                $.each(json.fixtures, function (key, value) {
                    var HomeTeamAvilableMarkup = '<span class="availableHomeTeam">',
                            AwayTeamAvailableMarkup = '<span class="availableAwayTeam">';

                    if (AllowedTeams.indexOf(value.ShortNameHome) == -1) {
                        HomeTeamAvilableMarkup = '<span class="unavilableHomeTeam">'
                    }
                    if (AllowedTeams.indexOf(value.ShortNameAway) == -1) {
                        AwayTeamAvailableMarkup = '<span class="unavilableAwayTeam">'
                    }

                    if (json.formguide) {
                        var HomeTeamFormHtml = json.formguide[value.HomeTeam].replaceAll("WIN,", " &#128994;").replaceAll("LOS,"," &#128308;").replaceAll("DRW,"," &#128993;")
                        var AwayTeamFormHtml = json.formguide[value.AwayTeam].replaceAll("WIN,", " &#128994;").replaceAll("LOS,"," &#128308;").replaceAll("DRW,"," &#128993;") 
                    }
                    else{
                        var HomeTeamFormHtml = ""
                        var AwayTeamFormHtml = ""
                    }

                    var KillerHomeTeamMarkup = ""
                    var KillerAwayTeamMarkup = ""
                    var killerSelected = 0
                    if (value.KillerTeam == 1){
                        killerSelected = 1
                        KillerHomeTeamMarkup += " &#x1f9e8;"
                    }
                    if (value.KillerTeam == 3){
                        killerSelected = 3
                        KillerAwayTeamMarkup += " &#x1f9e8;"
                    }

                    $("#upComingFixtureList").append(
                            '<li data-role="list-divider"><table><tr><td><span class="kickoffTime">'
                            + value.KickOffTime.substring(0, value.KickOffTime.length - 3) + '</span></td><td><image src="' + value.HomeCrestImg + '" width=30 height=30 /></td><td><span class="vsSeparator">vs</span> </td><td><image src="' + value.AwayCrestImg + '" width=30 height=30 /></td></tr></table>' +
                            '</li>' +
                            '<li><a href="#" onclick="updateSelection(' + value.FixtureId + ',\'' + value.HomeTeam + '\', \'' + value.AwayTeam + '\',' + 1 + "," + killerSelected + ')" >' + KillerHomeTeamMarkup + ' ' + HomeTeamAvilableMarkup + value.HomeTeam  + HomeTeamFormHtml  + '</a></li>' +
                            '<li><a href="#" onclick="updateSelection(' + value.FixtureId + ',\'' + value.HomeTeam + '\', \'' + value.AwayTeam + '\',' + 3 + "," + killerSelected + ')" >' + KillerAwayTeamMarkup + ' ' + AwayTeamAvailableMarkup + value.AwayTeam + AwayTeamFormHtml + '</a></li>'

                            )
                    console.log(value.HomeTeam + ", form:" + json.formguide[value.HomeTeam] )
                });
                $('#upComingFixtureList').listview("refresh");
                updateCountdown();
                document.addEventListener("DOMContentLoaded", updateCountdown);
            }
            else {
                showAlreadyPlayed(json);
            }

        }
    });


}


function updateSelection(fixid, homeTeam, awayTeam, selected, killer=false) {
    //TODO: update the currentSelection div
    var killerTeamMsg = "<br><br>Select {teamname} this week and if they win," +
            " you get to REMOVE a life from another player of your choice"
    $("#submitNow").unbind("click");
    switch (selected) {
        case 1:
            $('#csTeamWin').text(homeTeam + "(home) to beat " + awayTeam);
            if (killer==1){
                $('#csTeamWin').append(killerTeamMsg.replace("{teamname}", 
                homeTeam))
            }
            if (homeTeam == 'Brighton' && currentUsername == 'LordStyxofdeNordsoide'){
                $('#csTeamWin').append("<br>Really Styx? Brighton? Again?")
            }
            break;
        case 3:
            $('#csTeamWin').text(awayTeam + "(away) to beat " + homeTeam);
            if (killer==3){
                $('#csTeamWin').append(killerTeamMsg.replace("{teamname}", 
                awayTeam))
            }
            if (awayTeam == 'Brighton' && currentUsername == 'LordStyxofdeNordsoide'){
                $('#csTeamWin').append("<br>Really Styx? Brighton? Again?")
            }
            break;
            
    }

    $('#currentSelection').slideDown();


    //TODO: Fix bug - this gets called multiple times if user changes their selection mutliple times before hitting submit.
    $('#submitNow').click(function () {
        makeSubmission(fixid, selected)
    })
}

function cancelPrediction(predictionId) {
    if (confirm('Are you sure you want to cancel this prediction?')) {
        var predictionToCancel = {"predictionId": predictionId};
        var posting = $.post("restServices/cancelPredictionSvc.php", predictionToCancel);
        $.mobile.loading('show', {
            text: 'Loading',
            textVisible: false,
            theme: 'z',
            html: ""
        });
        posting.done(function (data) {
            console.log(JSON.stringify(data));
            if (data.ROWS_AFFECTED == 1) {
                $.mobile.loading('hide');
                $('#alreadyPredictedDetails').empty();
                $('#submitNow').show();
                $('#submitCancel').text('Cancel');
                loadUserOpts();
            }
            else {
                $.mobile.loading('hide');
            }
        })
    }
}

function makeSubmission(fixid, select)
{

    var selection = {"FixtureId": fixid, "prediction": select};
    //if (confirm('Are you sure you want to submit this prediction? \nOnce a prediction has been submitted it cannot be changed!')) {
    var posting = $.post("restServices/submitPredictionSvc.php", selection);
    $.mobile.loading('show', {
        text: 'Loading',
        textVisible: false,
        theme: 'z',
        html: ""
    });
    posting.done(function (data) {
        $.mobile.loading('hide');
        console.log(JSON.stringify(data));
        if (data.status == 1) {
            $('#csTeamWin').text("Your prediction for this week has been submitted. Good Luck!")
            $('#submitNow').hide();
            $('#submitCancel').empty();
            $('#submitCancel').text('Close');
            $('#upComingFixtureList').empty();
            $('#messageInformSelect').empty();
            loadUserOpts();
        }
        else {
            
            console.log((data.reason));
            console.log(data.reason[0].substring(data.reason[0].length - 13, data.reason[0].length));
            //TODO: Select/case for every possible reason type
            var UsrMsg = ""
            //TODO: Below should be a switch/case to handle more error types
            if (data.reason[0].substring(data.reason[0].length - 13, data.reason[0].length) == "ey 'UserTeam'") {
                UsrMsg = "You have already selected this team in a previous "
                        + " round of the competition";
            }

            if (data.reason[0].substring(data.reason[0].length - 13, data.reason[0].length) == "UserGameWeek'") {
                UsrMsg = "You have already submitted a prediction for this game week"
            }
            if (data.reason == "Payment Pending") {
                // redirect to payment page.
                UsrMsg = "Pay Tommy First - revolut 10 to @tommy5kit";
                $('#submitNow').css("display: none;");
                $('#submitCancel').css("display: none;");
                $('#goPayment').show();
            }

            if (data.reason == "You're out ") {
                // redirect to payment page.
                UsrMsg = "Eliminated";
                $('#submitNow').css("display: none;");
                $('#submitCancel').css("display: none;");
                $('goMyHistory').show();
            }

            //$('#currentSelection').empty();
            $('#csTeamWin').html("Cannot submit this prediciton<br> \n" + UsrMsg);


        }
    }).fail(function(jqXHR, textStatus, errorThrown){
        // exact same thing cos DONE never fires FFS    
        console.log("Error: " + JSON.stringify(jqXHR))
    });
    //}
}

function dropDynamite(){
    let check_timestamp = localStorage.getItem('updatedAt');
    var selection = {"user_last_update": check_timestamp, "drop_on_user": dynamiteTargetUser, "dynamite_id": user_dynamite_id};
    //if (confirm('Are you sure you want to submit this prediction? \nOnce a prediction has been submitted it cannot be changed!')) {
    console.log("Posting to restServices/submitDynamiteDrop.php, data = " + JSON.stringify(selection))
    var posting = $.post("restServices/submitDynamiteDrop.php", selection);
    $.mobile.loading('show', {
        text: 'Loading',
        textVisible: false,
        theme: 'z',
        html: ""
    });

    posting.done(function(data){
        console.log("POSTING WAS SUCCESSFUL: " + JSON.stringify(data))
        var post_throw_msg = "";
        $.mobile.loading('hide')
        if (data['reason'] == "stale data"){
            post_throw_msg = "Cannot drop now as another user has recently" 
            post_throw_msg += " dropped a dynamite, please click continue"
            post_throw_msg += " to refresh this page and see the latest info"
            $("#submitDynamiteCancel").remove()
            $("#submitDynamiteNow").text("Continue")
            $("#submitDynamiteNow").click(function(){location.reload();})

        }
        else if (data['reason']['lives_remaining'] < 1){
            post_throw_msg= data['reason']['player_hit'] + " is out.. good job!";
            $("#dynamite").fadeOut();
            $("#submitDynamiteNow").text("Continue")
            $("#submitDynamiteNow").click(function(){location.href = '/home.php#standings';})
        }
        else{
            post_throw_msg = "You took a life from " + dynamiteTargetFullName + ", they now have " + data['reason']['lives_remaining'] + " lives"
            $("#dynamite").fadeOut();
            $("#submitDynamiteNow").text("Continue")
            $("#submitDynamiteNow").click(function(){location.href = '/home.php#standings';})
        }
        $("#dynamite").fadeOut();
        // remove and refresh the player tiles
        $("#dynamite-drop-h3").text(post_throw_msg)
        $("#submitDynamiteCancel").remove()
        $("#submitDynamiteNow").text("Continue")
        $('#submitDynamiteNow').attr('onclick',             
            '$("#dynamite_page").trigger("pageinit"); ' +
            '$("#dynamiteSelection").slideUp(); ' +
            '$(".drop-target, .gone-target").remove();' +
            '$("#dynamite_page").trigger("pageinit");'
        );
        
    }).fail(function (jqXHR, textStatus, errorThrown) {
        console.log("Request failed: " + textStatus); // Error handling
    });

   /*posting.done(function (data) {
        $.mobile.loading('hide');
        console.log(JSON.stringify(data));
        if (data.status == 1) {
            $('#csTeamWin').text("Done, bla bla now has x lives remaining, good job")
        }
        else {
            
            console.log((data.reason));
            console.log(data.reason[0].substring(data.reason[0].length - 13, data.reason[0].length));
            //TODO: Select/case for every possible reason type
            var UsrMsg = ""
            //TODO: Below should be a switch/case to handle more error types
            if (data.reason[0] == "stale_info") {
                UsrMsg = "Someone else has dropped dynamite since you opened this page " +
                "click continue to refresh the options list and try again";
            }
            $('#csTeamWin').html("Cannot drop dynamite becuase <br> \n" + UsrMsg);


        }
    });*/

}

function showAlreadyPlayed(selectionData) {
    //$("#alreadyPredictedDetails").empty();
    //console.log("already played funciton hit")
    console.log(JSON.stringify(selectionData));
    console.log("Prediction to cancel=" + selectionData[0].PredictionID);
    chgPredLinkHtml = '<button onclick="cancelPrediction(' + selectionData[0].PredictionID + ')">Click Here to Cancel This Prediction</button>';
    $("#alreadyPredictedDetails").html("<h3> Your prediction for this round has been submitted </h3>" +
            "<p>Fixture: " + selectionData[0].HomeTeam + " v " +
            selectionData[0].AwayTeam + "<br/>You selected:  " + selectionData[0].PredictedTeam + "<br>" +
            "<br>" + chgPredLinkHtml + "<br>" +
            "<br>Best of Luck!</p>");
    $('#alreadyPredictedDetails').collapsible("refresh");

    $('#messageInformSelect').fadeOut('slow');
    $('#upComingFixtureList').fadeOut('slow');

}


function displayPlayerStandings() {
    $.ajax({
        'url':
                'restServices/userStandings.php',
        dataType: 'json',
        success: function (json) {


            $.each(json, function (key, value) {
                var markUp = "";
                lives_lost = 3 - value["lives"]
                lives_rem = value["lives"]
                ballshtml = "&#9917;&nbsp;".repeat(value["lives"])
                ballshtml += "&#10060;&nbsp;".repeat(lives_lost)
                if (value["CompStatus"] == "Playing") {
                    markUp = '<span class="activePlayerName">' + ballshtml;
                }
                else if (value["CompStatus"] == "Eliminated") {
                    markUp = '<span class="elimPlayerName">';
                }
                $('#playerStandingsList').append(
                        '<li><a href="#userHistory" onclick="userToView=\'' + value["username"] + '\'" >' + markUp + value["FullName"] + '</span></a></li>'
                        )

            });
            $('#playerStandingsList').listview("refresh");

        }
    });
}


function displayPlayersForDynamite() {
    $.ajax({
        'url':
                'restServices/userStandings.php',
        dataType: 'json',
        success: function (json) {
            $.each(json, function (key, value) {
                var player_tile_class = "";
                lives_lost = 3 - value["lives"]
                lives_rem = value["lives"]
                ballshtml = "&#9917;&nbsp;".repeat(value["lives"])
                ballshtml += "&#10060;&nbsp;".repeat(lives_lost)
                if (value["CompStatus"] == "Playing") {
                    player_tile_class = 'drop-target'
                }
                else if (value["CompStatus"] == "Eliminated") {
                    player_tile_class = 'gone-target';
                }
                $('#player-tile-targets').append(
                    '<div class="' + player_tile_class + '"' + 
                    'id="'+ value["FullName"] + '" user_attr="' + value["username"] +
                    '">' + value["FullName"] 
                    +  '<br>' + ballshtml + '</div>'
                )

            });
            $(function() {
            // Make the element draggable using jQuery UI
            $("#dynamite").draggable();
            
            // Make the target divs droppable
            $(".drop-target").droppable({
                accept: "#dynamite",
                hoverClass: "active", // Add the active class when dynamite is dragged over
                drop: function(event, ui) {
                        // Event handler for when the dynamite is dropped on the div
                        $('#dynamiteSelection').slideDown()
                        const dropTargetId = $(this).attr('id');
                        dynamiteTargetFullName = $(this).attr('id')
                        dynamiteTargetUser = $(this).attr('user_attr')
                        $("#dynamite-drop-h3").text("Drop 🧨 on " + dynamiteTargetFullName + "?") 
                        
                        
                        console.log(currentUsername + " drops dynamite on " + dynamiteTargetUser)
                    }
                });
            });
            $('#no-dynamite-msg').hide()
            $('#dynamite-drop-options').show() 
        }
    });
}


function displaySelectionsPostDeadline() {
    $.ajax({
        'url': 'restServices/getSelectionsPostDeadline.php',
        dataType: 'json',
        success: function (json) {
            if (json[0].TIME_PUBLIC) {
               $('#publicSelectionsListLabel').html("This week's predictions will appear here after the deadline");
            }
            else {
                $('#publicSelectionsListLabel').html("This week's predictions:");
                $('#messageInformSelect').html("Submission deadline for the current game week has passed")
                $.each(json, function (key, value) {
                    console.log(JSON.stringify(json));
                    var selectionMethodText;
                    if (value["EntryType"]=="AUTO"){
                        selectionMethodText="Auto-Pick*: ";
                    }
                    else {
                        selectionMethodText="Selected: ";
                    }
                    var dynamite = ""
                    if (value["KillerTeam"] != null){
                        if (value["PredictedTeam"] == value["HomeTeam"] && value["KillerTeam"] == 1){
                            dynamite=" &#x1f9e8;"
                        }
                        if (value["PredictedTeam"] == value["AwayTeam"] && value["KillerTeam"] == 3){
                            dynamite=" &#x1f9e8;"
                        }
                    }
                    $('#publicSelectionsList').append(
                            '<li data-role="list-divider">Player: ' + value["FullName"] + '</li><li>' + value["HomeTeam"] + ' vs ' + value["AwayTeam"] + " - " + formatDateTime(value["KickOffTime"]) + '</li><li>'+ selectionMethodText + '<strong>' + value["PredictedTeam"] + dynamite + '</strong></li>'
                            )
                });
                $('#publicSelectionsList').listview("refresh");
            }
        }
    });
}

function showDynamiteHist() {
    $('#dynamiteActionsList').empty();
    $.ajax({
        'url': 'restServices/getDynamiteDropHistory.php',
        success: function(data){
            $.each(data, function (key, value) {
                $('#dynamiteActionsList').append(
                    "<li>On " + value["updated_at"] + ", " +
                    value["SourceFullName"] + " threw 🧨 at " +
                    value["TargetFullName"] + "</li>"
                )
            })
        }
    })
}

function showPlayerHist(inUser) {
    $('#userHistoryList').empty();
    //console.log("Getting history for: " + inUser);
    $.ajax({
        'url': 'restServices/getUserPredictionHistory.php?player=' + inUser,
        dataType: 'json',
        success: function (json) {
            $('#histForUser').html(inUser)
            $.each(json, function (key, value) {
                var markUp = "";
                console.log(JSON.stringify(value));
                if (value["PredictedResult"] == 1) {
                    markUp = '<td style="background-color:green;"> win </td>';
                }
                else if (value["PredictedResult"] == 0) {
                    markUp = '<td style="background-color:red"> lose </td>';
                }
                else {
                    markUp = '<td style="background-color:orange"> pending </td>';
                }

                $('#userHistoryList').append(
                        '<li><table class="predictTable"> \n <tr><td class="predictTableLabel">Fixture Date/Time: </td><td>' + value["KickOffTime"] + '</td></tr>' +
                        '<tr><td class="predictTableLabel">Home Team: </td><td>' + value["HomeTeam"] +
                        '</td></tr><tr><td class="predictTableLabel">Away Team: </td><td>' + value["AwayTeam"] +
                        '</td></tr><tr><td class="predictTableLabel">Selected: </td><td>' + value["PredictedWinner"] + '</td></tr>' +
                        '<tr><td class="predictTableLabel">Result: </td>' + markUp + '</tr></table></li>'
                        )

            });
            $('#userHistoryList').listview("refresh");

        }
    });
}


function requestPassReset(data) {
    console.log(data);
    $.mobile.loading('show', {
        text: 'Loading',
        textVisible: true,
        theme: 'z',
        html: ""
    });
    var posting = $.post("restServices/requestPasswordReset.php", data);
    posting.done(function (responseData) {
        $.mobile.loading('hide');
        console.log(JSON.stringify(responseData));
        if (responseData["status"] == "success") {
            alert('An email has been sent to the registered email account with reset instructions')
        }
        else {
            alert('User does not exist')
        }
    })

}



function doPassReset(data) {
    console.log(data);
    $.mobile.loading('show', {
        text: 'Loading',
        textVisible: true,
        theme: 'z',
        html: ""
    });
    $.post("restServices/doPasswordReset.php", data, function (responseData) {
        $.mobile.loading('hide');
        console.log(JSON.stringify(responseData));
        if (responseData["status"] == "success") {
            alert("Password has been succesfully reset for " + responseData["reason"] + ", please log in using your new password");
            window.location = "login.php";
        }
        else
        {
            alert("Password could not be reset: " + responseData["reason"]);
        }
    })

}

function findEarliestKickoff() {
    const elements = document.querySelectorAll("#upComingFixtureList .kickoffTime");
    let earliestTime = null;

    elements.forEach(el => {
        const dateTime = new Date(el.textContent.trim().replace(" ", "T")); // Ensure valid format
        if (!earliestTime || dateTime < earliestTime) {
            earliestTime = dateTime;
        }
    });

    return earliestTime;
}

function updateCountdown() {
    const countdownSpan = document.getElementById("countdown_days_hours_min");
    const earliestTime = findEarliestKickoff();

    if (!earliestTime) {
        countdownSpan.textContent = "No info on next match kickoff time is available";
        return;
    }

    function tick() {
        const now = new Date();
        const diff = earliestTime - now;

        if (diff <= 0) {
            countdownSpan.textContent = "Deadline passed";
            clearInterval(interval);
            return;
        }

        const days = Math.floor(diff / (1000 * 60 * 60 * 24));
        const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((diff % (1000 * 60)) / 1000);

        countdownSpan.textContent = `Deadline in ${days} Days, ${hours} Hours, ${minutes} Minutes ${seconds} seconds:`;
    }

    tick(); // Run immediately
    const interval = setInterval(tick, 1000); // Update every minute
}
