<?php ?>
<h2>Last Man Standing: Competition Rules</h2>
    <ol>
        <li>
            <strong>Entry Fee:</strong>
            <ul>
                <li><A href="#paymentPage">Pay</A> your entry fee in Week 1. No additional payments are required afterward.</li>
                <li>All entry fees contribute to the prize pot.</li>
            </ul>
        </li>

        <li>
            <strong>Weekly Team Selection:</strong>
            <ul>
                <li>Each week, select a team playing a match that week.</li>
                <li>If your team <strong>wins</strong>, you advance. If your team <strong>loses</strong> or <strong>draws</strong>, you are eliminated.</li>
                <li>You may only select each team <strong>once</strong> during the competition.</li>
            </ul>
        </li>

        <li>
            <strong>Deadline for Selections:</strong>
            <ul>
                <li>All selections must be made before the <strong>kick-off of the first match</strong> of the game week.</li>
                <li>You can change your selection up until this deadline by logging into the competition website and selecting <em>“cancel prediction.”</em></li>
            </ul>
        </li>

        <li>
            <strong>Missed Submission:</strong>
            <ul>
                <li>If you fail to submit a selection, the system will automatically pick a <strong>randomly chosen team</strong> from the teams still available to you.</li>
            </ul>
        </li>

        <li>
            <strong>Winning the Competition:</strong>
            <ul>
                <li>The competition continues until only <strong>one player remains</strong>. This player wins the pot.</li>
                <li>If all remaining players lose in the same round, they all advance to the next round.</li>
                <li>If only <strong>two players remain</strong>, the competition ends if they both lose more than once while it remains just the two of them. In this case, the pot is split equally.</li>
            </ul>
        </li>
    </ol>


